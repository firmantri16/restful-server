<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Mahasiswa extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mahasiswa_model', 'mahasiswa');
        // $this->methods['index_post']['limit'] = 2;
        // $this->methods['index_get']['limit'] = 2;
        // $this->methods['index_put']['limit'] = 2;
        // $this->methods['index_delete']['limit'] = 2;
    }

    //create
    public function index_post()
    {
        $nrp = $this->post('nrp');
        $nama = $this->post('nama');
        $email = $this->post('email');
        $jurusan = $this->post('jurusan');

        $data = [
            'nrp' => $nrp,
            'nama' => $nama,
            'email' => $email,
            'jurusan' => $jurusan
        ];

        if ($nrp !== null || $nama !== null || $email !== null || $jurusan !== null) {
            if ($this->mahasiswa->createMahasiswa($data) > 0) { //ada mhs yg ditambahkan
                $this->response([
                    'status' => true,
                    'message' => 'Mahasiswa baru ditambahkan.'
                ], REST_Controller::HTTP_CREATED);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Gagal menambahkan data mahasiswa baru!'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Sediakan NRP, Nama, Email, dan Jurusan'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    //read
    public function index_get()
    {
        $id = $this->get('id');
        if ($id === null) {
            $mhs = $this->mahasiswa->getMahasiswa();
        } else {
            $mhs = $this->mahasiswa->getMahasiswa($id);
        }

        if ($mhs) {
            $this->response([
                'status' => true,
                'data' => $mhs
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data mahasiswa tidak ditemukan!'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    //update
    public function index_put()
    {
        $id = $this->put('id');
        $nrp = $this->put('nrp');
        $nama = $this->put('nama');
        $email = $this->put('email');
        $jurusan = $this->put('jurusan');

        if ($id !== null) {
            if ($nrp !== null && $nama !== null && $email !== null && $jurusan !== null) {
                $data = [
                    'nrp' => $nrp,
                    'nama' => $nama,
                    'email' => $email,
                    'jurusan' => $jurusan
                ];
                // var_dump($data);
                $result = $this->mahasiswa->updateMahasiswa($data, $id);

                if ($result > 0) { //ada data yg terupdate
                    $this->response([
                        'status' => true,
                        'message' => 'Data telah diperbaharui'
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Gagal memperbaharui data.'
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Sediakan nrp, nama, email, dan jurusan!'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Sediakan id data!'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    // delete
    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => false,
                'message' => 'Sediakan id data yang akan dihapus!.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $result = $this->mahasiswa->deleteMahasiswa(['id' => $id]);
            if ($result > 0) { //ada data yang terhapus
                $this->response([
                    'status' => true,
                    'message' => 'Data dengan id : ' . $id . ' terhapus'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Id : ' . $id . ' tidak ditemukan!'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
